import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import BookPreview from "./BookPreview";
import BookDetailModal from "./BookDetailsModal";

import { getBooksByFilter } from "../redux/selectors";
import { fetchBooks } from "../redux/actions";

const BookPreviewList = ({ books, fetchBooks, bookReducer }) => {
  // este hook se ejecuta despues de que el componente se ha
  // renderizado.
  useEffect(() => {
    fetchBooks();
  }, []);

  return books.loading ? (
    <h2>Loading books...</h2>
  ) : books.error ? (
    <h3>{books.error}</h3>
  ) : (
    <>
      <ul className="books-list">
	{
	  books.books.map((book) => {
	    return (
	      <BookPreview
		key={`book-${book.id}`}
		id={book.id}
		title={book.title}
		authors={book.authors}
		pageCount={book.pageCount}
		rate={book.rate}
		imgUrl={book.imgUrl}
	      />
	    )
	  })
	}
      </ul>
      {bookReducer.seeingDetails === true ? <BookDetailModal /> : <i></i>}
    </>
  );
};

const mapStateToProps = (state) => {
  // console.log('STATE... BookPreviewList');
  // console.log(state.booksReducer);
  const { bookReducer } = state;

  const { booksFilterReducer } = state;
  const books = getBooksByFilter(state, booksFilterReducer);

  return { books, bookReducer };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBooks: () => dispatch(fetchBooks())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookPreviewList);
