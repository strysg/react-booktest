import React from "react";
import { connect } from "react-redux";
import { setBooksFilter } from "../redux/actions";
import { BOOK_FIELD_FILTERS } from "../constants";

import BookSelect from './BookSelect';

const BookFilter = ({ activeFilter, setBooksFilter }) => {
  const currentFilter = BOOK_FIELD_FILTERS[activeFilter.by];
  console.log(currentFilter);
  return (
    <div className="books-filter">

      <BookSelect />

      <input
        type="text"
        key={`book-filter=${currentFilter}`}
        onChange={(event) => {
          /* 
            este setBooksFilter es el que recibe el componente BookFilter
            previamente esta ha sido 'despacahado' (dispatch) mediante connect()
           */
          setBooksFilter({ value: event.target.value });
        }}
      />
    </div>
  );
};

const mapStateToProps = state => {
  return { activeFilter: state.booksFilterReducer };
};

export default connect(
  mapStateToProps,     // subscribe
  { setBooksFilter }   // dispatch
)(BookFilter);
