import React from "react";

import Badge from "react-bootstrap/Badge";

const RateStars = props => {
  return <Badge variant="primary">★</Badge>;
};

export default RateStars;
