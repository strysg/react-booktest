import React from "react";
import { connect } from 'react-redux';
import { setBooksFilter } from '../redux/actions';
import { BOOK_FIELD_FILTERS } from "../constants";

const BookSelect = ({ activeFilter, setBooksFilter }) => {
  return (
      <React.Fragment>
        <span>Select book filter:</span>
        <select onChange={(event) => {
          setBooksFilter({ by: event.target.value });
        }}>
	  <option value={BOOK_FIELD_FILTERS.ALL}>{BOOK_FIELD_FILTERS.ALL}</option>
          <option value={BOOK_FIELD_FILTERS.TITLE}>{BOOK_FIELD_FILTERS.TITLE}</option>
          <option value={BOOK_FIELD_FILTERS.AUTHOR}>{BOOK_FIELD_FILTERS.AUTHOR}</option>
	</select>
      </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  const { booksFilterReducer } = state;
  return { activeFilter: booksFilterReducer };
};
export default connect (
  mapStateToProps,
  { setBooksFilter }
)(BookSelect);
