import React from "react";
import { connect } from "react-redux";
import { seeUserProfile, userLogout } from '../redux/actions';

import UserProfileModal from "./UserProfileModal";

import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from 'react-bootstrap/DropdownButton';

const UserDropdown = ({ userData, seeUserProfile }) => {
  return (
    <>
      <DropdownButton
        alignRight
        title={userData.username}
        id="dropdown-user-menu">
        <Dropdown.Item onClick={() => {
			 seeUserProfile(userData);
		       }}>
          Profile
        </Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item onClick={() => {
			 console.log('logout');
			 userLogout(userData);
		       }}>
          Logout
        </Dropdown.Item>
      </DropdownButton>
      {userData.seeingProfile === true ? <UserProfileModal /> : <i></i>}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.setUserProfileReducer,
  };
}

export default connect (
  mapStateToProps,
  { seeUserProfile }
)(UserDropdown);
