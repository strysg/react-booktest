import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchBookById, closeBookDetails, rateBook } from "../redux/actions";
import utils from "../utils";

import RateStars from './RateStars';

import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";

const BookDetailModal = (
  { book,
    closeBookDetails, fetchBookById,
    rateBook
  }) => {

  useEffect(() => {
    // console.log('useEffect', book.id);
    fetchBookById(book.id);
  }, []);

  return (
    <Modal
      show={true}
      onHide={() => {
	closeBookDetails();
      }}>
      <Modal.Header closeButton>
	<Modal.Title>
          {book.title}
	</Modal.Title>
      </Modal.Header>
      <Modal.Body>
	<ListGroup>
	  <ListGroup.Item variant="primary">
	    By: <big>{book.authors.join(' , ')}</big>
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <img
	      alt="libro"
              width="256"
	      src={book.imgUrl} />
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Description</strong>
	    <p>{book.description}</p>
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Pages</strong>: {book.pageCount}
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Rate</strong>
	    {
	      utils.range(0, parseInt(book.rate)).map(star => {
		return <RateStars key={`stars-${parseInt(Math.random()*1000)}`} />
	      }) 
	    } {book.rate}
	    <p>
	      <small>
		<strong>valorations:</strong>
		({book.rateCount})
	      </small>
	    </p>
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <small>Rate the book:</small>
            <ButtonGroup aria-label="Rate">
	      <Button variant="secondary"
		      onClick={() => { rateBook(book.id, 1)}}
	      >1
	      </Button>
	      <Button variant="secondary"
		      onClick={() => { rateBook(book.id, 2)}}
	      >2</Button>
	      <Button variant="primary"
		      onClick={() => { rateBook(book.id, 3)}}
	      >3</Button>
	      <Button variant="primary"
		      onClick={() => { rateBook(book.id, 4)}}
	      >4</Button>
	      <Button variant="success"
		      onClick={() => { rateBook(book.id, 5)}}
	      >5</Button>
	    </ButtonGroup>
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Categories:</strong> {book.categories.join(' , ')}
	  </ListGroup.Item>
	</ListGroup>
      </Modal.Body>
      <Modal.Footer>
	<Button
	  variant="secondary"
	  onClick={() => {
	    closeBookDetails();
	  }}
	>
	  Close
	</Button>
      </Modal.Footer>
    </Modal>
  );
}

const mapStateToProps = (state) => {
  return {
    book: state.bookReducer
  };
}

const mapDispatchToProps = dispatch => {
  return {
    fetchBookById: (id) => dispatch(fetchBookById(id)),
    rateBook: (id, rate) => dispatch(rateBook(id, rate)),
    closeBookDetails
  };
};


export default connect (
  mapStateToProps,
  mapDispatchToProps
)(BookDetailModal);
