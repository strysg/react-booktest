import React from "react";
import { connect } from "react-redux";
import { saveUserProfile, closeUserProfile } from '../redux/actions';

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup"

const UserProfileModal = ({ userData, closeUserProfile, saveUserProfile }) => {
  return (
    <Modal
      show={true}
      onHide={() => {
	closeUserProfile(userData);
      }}>
      <Modal.Header closeButton>
	<Modal.Title>
          User's Profile: {userData.username}
	</Modal.Title>
      </Modal.Header>
      <Modal.Body>
	<ListGroup>
	  <ListGroup.Item variant="secondary">
	    <big>{userData.username}</big>
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Name</strong>: {userData.name}
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <strong>Email:</strong> {userData.email}
	  </ListGroup.Item>
	  <ListGroup.Item>
	    <img
	      alt="libro"
              width="32"
	      src={userData.avatarUrl} />
	  </ListGroup.Item>
	  <ListGroup.Item variant="secondary">
	    Last Login: {userData.lastLogin}
	  </ListGroup.Item>
	</ListGroup>
      </Modal.Body>
      <Modal.Footer>
	<Button
	  variant="secondary"
	  onClick={() => {
	    closeUserProfile(userData);
	  }}
	>Close</Button>
	<Button variant="primary">Save changes</Button>
      </Modal.Footer>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.setUserProfileReducer,
  };
};

export default connect (
  mapStateToProps,
  { closeUserProfile, saveUserProfile }
)(UserProfileModal);
