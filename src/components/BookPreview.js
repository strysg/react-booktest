import React from "react";
import { connect } from "react-redux";
import { addBookPreview, seeBookDetails } from "../redux/actions";
import utils from "../utils";

import RateStars from './RateStars';

import Badge from "react-bootstrap/Badge";

const BookPreview = ({
  id, title, authors, imgUrl, pageCount, rate,
  seeBookDetails
}) => {
  // console.log('BookPreview');
  // console.log(props);
  // console.log('---');
  return (
    <span className="book-preview">
      <hr />
      <img
        src={imgUrl}
        alt="libro"
        width="32"/>
      <strong id={`book-${id}`}>
	<a
	  href={`#book-${id}`}
	  onClick={() => {
	    seeBookDetails({id, title, authors, imgUrl, pageCount, rate});
	  }}>
	  {title}
	</a>
      </strong><br/>
      <strong> Authors:</strong>
      <i> {authors.join(' , ')}</i>
      <br/>
      <strong>Page count:</strong> <Badge variant="secondary">{pageCount}</Badge><br/>
      <strong>rate:</strong>
      {utils.range(0, parseInt(rate)).map((star, i) => {
        return <RateStars key={`stars-${id}-${i}`}/>;
      })}
    </span>
  );
};

const mapStateToProps = (state) => {
  return {
    book: state.bookReducer,
  };
};

export default connect(
  //null,
  mapStateToProps,
  { addBookPreview, seeBookDetails },
)(BookPreview);
