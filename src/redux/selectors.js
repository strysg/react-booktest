import { BOOK_FIELD_FILTERS } from "../constants";

export const getBooksState = store => store.booksReducer;

export const getBookList = (store, id) => {
  return getBooksState(store) ? getBooksState(store) : [];
};

// TODO: corregir
export const getBookById = (store, id) => {
  return getBooksState(store) ? { ...getBooksState(store).byIds[id], id } : {};
};

export const getBooksByFilter = (store, filter) => {
  const allBooks = getBookList(store);
  // console.log('getBooksByFilter:::;');
  // console.log(allBooks);
  if (filter.value.length <= 2) {
    return allBooks;
  }
  switch (filter.by) {
  case BOOK_FIELD_FILTERS.TITLE:
    {
      const books = allBooks.books.filter((book) => {
	return book.title.toLowerCase().startsWith(
	  filter.value.trim().toLowerCase());
      });
      return { books };
    }
    case BOOK_FIELD_FILTERS.AUTHOR: {
      const books = [];
      for (const book of allBooks.books ) {
        if (
          book.authors.some(
	    author => author.toLowerCase()
              .includes(filter.value.trim().toLowerCase()))
        ) {
          books.push(book);
        }
      }
      return { books };
    }
    case BOOK_FIELD_FILTERS.NO_FILTER: {
      return allBooks.books;
    }
    default: {
      return allBooks.books;
    }
  }
};
