import {
  SEE_USER_PROFILE,
  USER_LOGOUT,
  SAVE_USER_PROFILE,
  CLOSE_USER_PROFILE,
} from "../actionTypes";

export const seeUserProfile = (userData) => {
  return {
    type: SEE_USER_PROFILE,
    payload: userData
  };
};

export const saveUserProfile = (userData) => {
  return {
    type: SAVE_USER_PROFILE,
    payload: userData
  };
};

export const closeUserProfile = (userData) => {
  return {
    type: CLOSE_USER_PROFILE,
    payload: userData
  };
};

export const userLogout = (userData) => {
  return {
    type: USER_LOGOUT,
    payload: userData
  };
};
