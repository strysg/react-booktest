import axios from "axios";
import { API_URLS } from "../../constants";

import {
  ADD_BOOK_PREVIEW,
  SET_BOOK_FILTER,
  FETCH_BOOKS,
  FETCH_BOOKS_ERROR,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOK,
  FETCH_BOOK_ERROR,
  FETCH_BOOK_SUCCESS,
  SEE_BOOK_DETAILS,
  CLOSE_BOOK_DETAILS,
  RATE_BOOK,
  RATE_BOOK_SUCCESS,
  RATE_BOOK_ERROR,
} from "../actionTypes";

// this is an action creator
// it is a function that returns a function that dispatchs actions
export const fetchBooks = () => {
  // returns a function
  return (dispatch) => {
    dispatch(fetchBooksRequest());
    axios.get(API_URLS.bookList)
      .then((resp) => {
	if (resp.status !== 200) {
	  dispatch(fetchBooksError(resp.error));
	}
	dispatch(fetchBooksSuccess(resp.data))
      })
      .catch((error) => {
	dispatch(fetchBooksError(error.message));
      });
  };
}

export const fetchBooksRequest = () => {
  return {
    type: FETCH_BOOKS
  };
}

export const fetchBooksError = (error) => {
  return {
    type: FETCH_BOOKS_ERROR,
    payload: error,
  };
};

export const fetchBooksSuccess = (books) => {
  return {
    type: FETCH_BOOKS_SUCCESS,
    payload: books,
  };
};

export const addBookPreview = data => {
  return {
    type: ADD_BOOK_PREVIEW,
    payload: {
      id: data.id,
      title: data.title,
      authors: data.authors,
      pageCount: data.pageCount,
      rate: data.rate,
      userState: data.userState
    }
  };
};

// individual book
export const fetchBookRequest = () => {
  return {
    type: FETCH_BOOK
  };
}

export const fetchBookError = (error) => {
  return {
    type: FETCH_BOOK_ERROR,
    payload: error,
  };
};

export const fetchBookSuccess = (books) => {
  return {
    type: FETCH_BOOK_SUCCESS,
    payload: books,
  };
};

export const fetchBookById = (id) => {
  return (dispatch) => {
    dispatch(fetchBookRequest);
    axios.get(`${API_URLS.bookId}${id}`)
    .then(resp => {
      if (resp.status !== 200) {
	dispatch(fetchBookError(resp.error));
      }
      dispatch(fetchBookSuccess(resp.data));
    })
    .catch(error => {
      dispatch(fetchBookError(error.message));
    });
  }
}

export const seeBookDetails = (book) => {
  return {
    type: SEE_BOOK_DETAILS,
    payload: book
  };
};

export const closeBookDetails = () => {
  return {
    type: CLOSE_BOOK_DETAILS,
  };
};

// Add rate to Book
export const rateBookRequest = () => {
  return {
    type: RATE_BOOK,
  }
};

export const rateBookSuccess = (book) => {
  return {
    type: RATE_BOOK_SUCCESS,
    payload: book
  };
};

export const rateBookError = (error) => {
  return {
    type: RATE_BOOK_ERROR,
    payload: error
  };
};

export const rateBook = (id, rate) => {
  return dispatch => {
    dispatch(rateBookRequest());
    axios.post(`${API_URLS.bookRate}${id}/${rate}`, {})
      .then(resp => {
	if (resp.status !== 200) {
	  dispatch(rateBookError(resp.error));
	}
	dispatch(rateBookSuccess(resp.data));
      })
      .catch(error => {
	dispatch(rateBookError(error.message));
      });
  };
}
// filter
export const setBooksFilter = (filter) => {
  // console.log('action setBooksFilter');
  // console.log({
  //   type: SET_BOOK_FILTER,
  //   payload: filter,
  // });
  return {
    type: SET_BOOK_FILTER,
    payload: filter,
  };
};
