import { SEE_USER_PROFILE,
	 CLOSE_USER_PROFILE,
	 USER_LOGOUT,
	 SAVE_USER_PROFILE
       } from '../actionTypes';

const initialState = {
  username: 'Anonymous',
  id: 0,
  name: 'Anónimo',
  seeingProfile: false,
  profile: {
    avatarUrl: 'https://covers.openlibrary.org/b/id/7363356-L.jpg',
    isAdmin: false,
    email: '--',
    lastLogin: '',
  }
};

const setUserProfileReducer = (state=initialState, action) => {
  // console.log('reducer user profile', action);
  switch(action.type) {
    case SEE_USER_PROFILE: {
      return {
        ...state,
        seeingProfile: true
      };
    }
    case SAVE_USER_PROFILE: {
      return {
	...state,
	name: state.name,
	avatarUrl: state.avatarUrl || initialState.avatarUrl,
	email: state.email || initialState.email
      };
    }
  case CLOSE_USER_PROFILE: {
      return {
        ...state,
        seeingProfile: false
      };
    }
    case USER_LOGOUT: {
      return {
        ...state, // previous
        ...initialState // replace new to initial
      };
    }
    default: {
      return state;
    }
  }
};

export default setUserProfileReducer;
