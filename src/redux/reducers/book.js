import {
  FETCH_BOOK, FETCH_BOOK_ERROR, FETCH_BOOK_SUCCESS,
  SEE_BOOK_DETAILS, CLOSE_BOOK_DETAILS,
  RATE_BOOK, RATE_BOOK_ERROR, RATE_BOOK_SUCCESS
} from '../actionTypes';

const initialState = {
  loading: false,
  error: '',
  seeingDetails: false,
  id: 0,
  title: "",
  description: "default",
  imgUrl: "",
  authors: [],
  pageCount: 0,
  rate: 0,
  categories: [],
  userState: "",
};

const bookReducer = (state=initialState, action) => {
  switch(action.type) {
    case FETCH_BOOK: {
      return {
	...state,
	loading: true,
	error: '',
      };
    }
    case FETCH_BOOK_SUCCESS: {
      return {
	...state,
	loading: false,
	error: '',
	...action.payload
      }
    }
    case FETCH_BOOK_ERROR: {
      return {
	...state,
	loading: false,
	error: action.payload
      }
    }
    case SEE_BOOK_DETAILS: {
      const book = action.payload;
      return {
        ...state,
        seeingDetails: true,
        ...book // changes book being viewed
      }
    }
    case CLOSE_BOOK_DETAILS: {
      console.log('...,,,..,,.');
      return {
        ...state,
        seeingDetails: false
      };
    }
    case RATE_BOOK: {
      return {
	...state,
	// loading: true,
	error: ''
      }
    }
    case RATE_BOOK_ERROR: {
      return {
	...state,
	// loading: false,
	error: action.payload
      }
    }
    case RATE_BOOK_SUCCESS: {
      const book = action.payload;
      return {
	...state,
	// loading: false,
	error: '',
	...book // updates book
      }
    }
    default: {
      return state;
    }
  }
};

export default bookReducer;
