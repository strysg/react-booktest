import { combineReducers } from "redux";
//import visibilityFilter from "./visibilityFilter";
//import todos from "./todos";
import bookReducer from "./book";
import booksReducer from "./books";
import booksFilterReducer from "./booksFilter";
import setUserProfileReducer  from './userData';

export default combineReducers({
  bookReducer,
  booksReducer,
  booksFilterReducer,
  setUserProfileReducer,
  //visibilityFilter
});
