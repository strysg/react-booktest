import { SET_BOOK_FILTER } from "../actionTypes";
import { BOOK_FIELD_FILTERS } from "../../constants";

const initialState = {
  by: BOOK_FIELD_FILTERS.NO_FILTER,
  value: ''
};

const booksFilterReducer = (state = initialState, action) => {
  // console.log('reducer action:');
  // console.log(action);
  switch (action.type) {
    case SET_BOOK_FILTER: {
      return {
        ...state,
        by: action.payload.by || state.by,
        value: action.payload.value || state.value
      };
      // return action.payload.filter;
    }
    default: {
      return state;
    }
  }
};

export default booksFilterReducer;
