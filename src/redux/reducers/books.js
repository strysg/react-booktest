import {
  ADD_BOOK_PREVIEW,
  FETCH_BOOKS, FETCH_BOOKS_ERROR, FETCH_BOOKS_SUCCESS,
} from '../actionTypes';

/*
A reducer is a function that receives the current state and an action object, decides how to update the state if necessary, and returns the new state: (state, action) => newState. You can think of a reducer as an event listener which handles events based on the received action (event) type.
*/

const initialState = {
  loading: false,
  error: '',
  books: [
    {
      id: 1,
      title: "Little Blue Truck's Valentine",
      authors: ['ALice Chertle', 'Jum McElmurry'],
      pageCount: 187,
      rate: 1,
      categories: ['comedy', 'fiction'],
      userState: "",
    },
    {
      id: 2,
      title: "Jugando con Raspberry pi",
      authors: ['Adismon Luter', 'Jum McElmurry'],
      pageCount: 152,
      rate: 3,
      categories: ['science', 'technology', 'educational'],
      userState: "",
    },
    {
      id: 3,
      title: "Meditations",
      authors: ['Marcus Aurelius'],
      pageCount: 521,
      rate: 4,
      categories: ['psychological', 'educational'],
      userState: "",
    },
    {
      id: 10,
      title: "Electrónica básica",
      authors: ['Juan Alberto Guiterrez', 'Rom Mock'],
      pageCount: 311,
      rate: 3,
      categories: ['technology', 'educational'],
      userState: "",
    },
    {
      id: 11,
      title: "Manual de construcción civil inicial",
      authors: ['Luriel Espinoza', 'Dora Motenloio', 'Esteban Hugales'],
      pageCount: 77,
      rate: 3.3,
      categories: ['construction', 'educational'],
      userState: "",
    }
  ]
};

const booksReducer = (state=initialState, action) => {
  switch(action.type) {
    case FETCH_BOOKS: {
      return {
	...state,
	loading: true,
	error: '',
      };
    }
    case FETCH_BOOKS_SUCCESS: {
      return {
	...state,
	loading: false,
	error: '',
	books: action.payload
      };
    }
    case FETCH_BOOKS_ERROR: {
      return {
	...state,
	loading: false,
	error: action.payload,
      }
    }
    default: {
      return state;
    }
  }
};

export default booksReducer;

// receives the state and the action
// deprecated
// export default function (state = initialState, action) {
//   switch (action.type) {
//     case ADD_BOOK_PREVIEW: {
//       const { id, title, authors,
//               pageCount, rate, userState } = action.payload;
//       return {
//         ...state, // copying
//         allIds: [...state.allIds, id],
//         byIds: {
//           ...state.byIds,
//           [id]: {
//             id,
//             title,
//             authors,
//             pageCount,
//             rate,
//             userState
//           }
//         },
//       };
//     }
//   default:
//     return state;
//   }
// };
// 
