export const BOOK_FIELD_FILTERS = {
  NO_FILTER: 'NO FILTER',
  TITLE: 'TITLE',
  AUTHOR: 'AUTHOR'
};

export const USER_BOOK_STATES = {
  READ: 'Already read',
  TO_READ: 'Not read',
};

export const API_BASE_URL = "https://localhost:44318/api/";

export const API_URLS = {
  bookList: `${API_BASE_URL}books`,
  bookId: `${API_BASE_URL}books/`,
  bookDelete: `${API_BASE_URL}books/`,
  bookPut: `${API_BASE_URL}books/`,
  bookPost: `${API_BASE_URL}books/`,
  bookRate: `${API_BASE_URL}books/rate/`,
};
