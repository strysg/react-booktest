import React from "react";
import BookPreviewList from "./components/BookPreviewList";
import BookFilter from './components/BookFilter';
import UserDropdown from './components/UserDropdown';

import 'bootstrap/dist/css/bootstrap.min.css';
// import "./styles.css";

import Container from 'react-bootstrap/Container';

export default function App() {
  return (
    <Container fluid="md">
      <UserDropdown />
      <h1>Lista de libros</h1>
      <BookFilter />
      <BookPreviewList />
      {/* <AddTodo /> */}
      {/* <TodoList /> */}
      {/* <VisibilityFilters /> */}
    </Container>
  );
}
